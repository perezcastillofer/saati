import random

class StockPiece(object):
    """
    Stock piece in a map
    """

    def __init__(self, length: float, height: float, quality_level: str,
                 positionX: int, positionY: int):

        self.id_stock_piece = random.randint(1,100000)
        self.height = height
        self.length = length
        self.quality_level = quality_level
        self.positionX = positionX
        self.positionY = positionY
        self.id_father_stock_piece = False

    def get_area(self):
        """
        Calculates area in m2
        """
        return self.height * self.length



    def check_residual(self):
        pass

    def assign_order(self):
        pass



class Order(object):
    """
    costumer order
    """

    def __init__(self, costumer: str, raw_material: str, min_height: float,
                 max_height: float, min_quantity: float, max_quantity: float,
                 min_cut_length: float, max_cut_length: float):

        self.id_order = random.randint(1,100000)
        self.costumer = costumer
        self.raw_material = raw_material
        self.min_height = min_height
        self.max_height = max_height
        self.min_quantity = min_quantity
        self.max_quantity = max_quantity
        self.min_cut_length = min_cut_length
        self.max_cut_length = max_cut_length

        self.stockpieces = set([])

    def add_stock_piece(self, stock_piece: StockPiece):
        """

        :param stock_piece:
        :return: Adds a stock piece to the order
        """
        self.stockpieces.add(stock_piece)

    def order_area(self):
        """
        Calculates the area taking in consideration the stock pieces
        :return: order area
        """
        for stock_piece in self.stockpieces:
            return sum(stock_piece.get_area())


    def get_assigned_quantity(self, id_stock_piece, height, length):

        stockpiece = StockPiece(id_stock_piece,height, length)
        self.stockpieces[id_stock_piece] = stockpiece
        return self.stockpieces[id_stock_piece]

    def calcdelay(self):
        pass




class Map(object):
    """
    Initial, intermediate or final stage of the fabric sheet
    """

    def __init__(self, map_type: str, real):

        self.map_type = map_type
        self.real = real

        self.stock_pieces = ([])


    def add_stock_piece(self, stock_piece: StockPiece):
        """
        Adds a stock piece to the map
        """
        self.stockpieces.add(stock_piece)

    def delete_stock_piece(self, stock_piece: StockPiece):
        """
        Deletes a stock piece from the map
        """
        self.stockpieces.remove(stock_piece)

class FabricSheet(object):
    """
    represents a single fabric sheet
    """

    def __init__(self, raw_material: str, length: float, height: float, department: str,
                 weaving_forecast):
        self.id_fabric_sheet = random.randint(1,100000)
        self.raw_material = raw_material
        self.height = height
        self.length = length
        self.department = department
        self.weaving_forecast = weaving_forecast

        self.maps = ([])

    def select_map(self):
        pass

    def add_map(self, map: Map):

        self.maps.add(map.map_type)



FirstOrder = Order("Zara", "wool", 20, 10, 3, 8, 17, 40)

FabricSheet_1 = FabricSheet("wool", 30, 40, "women", True)

SP_1 = StockPiece(2,4,"med",5,6)
SP_2 = StockPiece(3,5,"excellent",8,9)

FirstOrder.add_stock_piece(SP_1)
FirstOrder.add_stock_piece(SP_2)
FirstOrder.order_area()

